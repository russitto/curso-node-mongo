/* eslint-disable semi */
"use strict";

const fdebug = require("./fdebug");
const debug = fdebug("movies:lib:movies");
const request = require("request");

function Movies(main) {
    this.db = main.db;
    debug('init');
}

function imdbSearch(title, cb) {
  const url = `http://www.omdbapi.com/?t=${title}&plot=short&r=json`;
  request.get(url, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const data = JSON.parse(body);
      if (data.Response != 'False') {
        var movie = {
          name: data.Title,
          image: data.Poster
        };
        cb(movie);
      }
    }
    cb(false);
  });
}

Movies.prototype.search = function(obj){
    var self = this;

    debug("search called: "+JSON.stringify(obj));

    return new Promise((resolve, rejec)=>{

        let query = {};

        if(obj.title) query.name = new RegExp(obj.title, 'i');
        // if(obj.year) query.Year = obj.year;
        // if(obj.id) query.imdbID = obj.id;

        self.db.movies.find(query, {}, (err, docs)=>{
          if (err) {
            return reject(err)
          }
          debug(docs.length);
          debug(obj.title);
          if (docs.length == 0 && obj.title) {
            imdbSearch(obj.title, function (movie) {
              if (movie) {
                self.db.movies.insert(movie, {}, (err, doc)=>{
                    err ? reject(err) : resolve([doc]);
                })
              }
            });
            // http://www.omdbapi.com/?t=rocky&plot=short&r=json
          } else {
            return resolve(docs);
          }
        })
    });
}

Movies.prototype.create = function(obj){
    var self = this;

    debug("search called: "+JSON.stringify(obj));

    return new Promise((resolve, rejec)=>{

        self.db.movies.insert(obj, {}, (err, doc)=>{
            err ? reject(err) : resolve(doc);
        })
    });
}

module.exports = Movies;
