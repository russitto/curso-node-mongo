#!/bin/env node

'use strict';

console.log('Servidor web 1.0, clase 2')

const http = require('http'),
  mainServer = require('./lib/server'),
  myServer = new mainServer(),
  server = http.createServer(myServer.Request)

myServer.get('/time', (req, res) => {
  res.end(new Date().toString())
})

server.listen(8000)
