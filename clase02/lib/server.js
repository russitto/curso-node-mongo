'use strict';

const fs = require('fs'),
  publicDirectory = process.cwd() + '/public',
  dynamicRoutes = {}

/*
function server(req, res) {
  this.req = req
  this.res = res

  console.log(req.url)

  let file = '',
    content = ''
  // console.log(req.headers, req.url)
  if (req.url == '/') file = '/index.html'
  try {
    content = fs.readFileSync(pub + file)
  } catch (e) {
    content = fs.readFileSync(pub + '/404.html')
  }
  res.end(content)
  // res.end('gracias x su visita')
}
*/

function server() {
  console.log('new server')
  this.Request = (req, res) => {
    console.log('new request')
    new Request(req, res)
  }
}

server.prototype.get = function(path, cb) {
	dynamicRoutes[path]=cb;
}

function Request(req, res){
	this.req = req;
	this.res = res;
 	console.log(req.url);
  
  if(!this.staticFile()){
  	if(!this.dynamicFile()){
  		this.notFound();
  	}
  }
}
Request.prototype.staticFile = function(){
	console.log("is static?");
	var _this = this;
	let file="";
  var content="";
  if(_this.req.url=="/") file="/index.html";
  try{
  	content = fs.readFileSync(publicDirectory+file);
  	return _this.res.end(content);
  }catch(e){
  	return null;
  }
 
}

Request.prototype.dynamicFile = function(){
	console.log("is dynamic?");
  if (typeof dynamicRoutes[this.req.url] != 'undefined') {
    dynamicRoutes[this.req.url](this.req, this.res)
    return true
  }
	return null;
}

Request.prototype.notFound = function(){
	console.log("404");
	this.res.end(fs.readFileSync(publicDirectory+"/404.html"));
}

module.exports = server
