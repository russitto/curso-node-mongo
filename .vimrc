syntax on
set guioptions=
colo darkblue
set number
set relativenumber
set cursorline
set nobackup
set tabstop=2
set expandtab
set shiftwidth=2
set ignorecase
set cindent
set autoindent
set smartindent
set wildmenu
set wildmode=longest,list,full
set scrolloff=5
set encoding=utf-8
set fileencoding=utf-8
set laststatus=2
set noshowmode
set incsearch
set hlsearch

" remap leader
let mapleader = "\<Space>"

" save
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>x :x<CR>
nnoremap ; :
nnoremap : ;
